#! /usr/bin/env tcl

package require critcl
if {![critcl::compiling]} {
    error "Unable to build project, no proper compiler found."
}

critcl::tcl 8.6
critcl::cflags -DWLR_USE_UNSTABLE
critcl::cheaders   -I/usr/include
critcl::clibraries -L/usr/lib/x86_64-linux-gnu
critcl::clibraries -lwayland-client
critcl::include wayland-client.h
critcl::include wlr/backend.h

proc opaquePointerType {type} {
    critcl::argtype $type "
        sscanf(Tcl_GetString(@@), \"($type) 0x%p\", &@A);
    " $type

    critcl::resulttype $type "
        Tcl_SetObjResult(interp, Tcl_ObjPrintf(\"($type) 0x%lx\", (uintptr_t) rv));
        return TCL_OK;
    " $type
}
opaquePointerType void*

critcl::cproc wl_display_connect {} void* {
    return wl_display_connect(NULL);
}

critcl::cproc wl_display_get_registry {void* display} void* {
    return wl_display_get_registry(display);
}

critcl::msg -nonewline { Building ...}
if {![critcl::load]} {
    error "Building and loading the project failed."
}

package provide wayland 1
